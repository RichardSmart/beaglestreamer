#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

#ifndef CAMSTREAM_CS
#define CAMSTREAM_CS

enum STREAM_STATES_CS
{
    UNINIT_CS,
    INIT_CS,
    DEINIT_CS,
    INIT_COMP_CS,    
    START_STREAM_CS,    
    STREAM_ACTIVE_CS,
    STOP_STREAM_CS,
    IDLE_CS,
    ERROR_CS,    
};

enum ERROR_STATES_CS
{
    OPENERR_CS,
    CAPQRYERR_CS,
    STREAMERR_CS,
    FMTERR_CS,
    REQBUFERR_CS,
    QRYBUFERR_CS,
    MEMMAPERR_CS,
};

struct cam_stream_config
{
    int storage_loc;
    int file_format;    
};

// Public interface
void *CamStreamerStateMachine(void *args);
int GetState_CS(void);

// Module functions
static int InitModule_CS(struct cam_stream_config *config);
static int DeinitModule_CS();
static int BeginStreaming_CS(void);
static int StopStreaming_CS(void);
static int GetFrame_CS(void);
static int HandleError_CS(int error_val);

#endif
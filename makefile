INC=-I/usr/include -I./inc

OBJS= ./src/main.c ./src/cam_streaming.c ./src/cam_net.c
OBJ_NAME=cam_streamer.out

CC=gcc
COMPILER_FLAGS= -g -std=gnu11
LINKER_FLAGS=-lv4l1 -lv4l2 -lSDL2 -lSDL2_image -pthread

all : $(OBJS)
	$(CC) $(OBJS) $(INC) $(COMPILER_FLAGS) -o $(OBJ_NAME) $(LINKER_FLAGS)

#main: ./src/main.c
#		gcc -g $(INC) -o cam_streamer.out ./src/main.c -lv4l1 -lv4l2 -lSDL2 -lSDL2_image
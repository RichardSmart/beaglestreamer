#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>
#include <pthread.h>
#include "cam_streaming.h"
#include "main.h"

extern bool cam_active;
extern bool cam_thread_close;
extern char *frame_buf;
extern struct frame_details frame_det;
extern pthread_mutex_t frame_mtx;

int fd;
int cur_state;
int error_val;
struct v4l2_capability cap;
struct v4l2_format format;
struct v4l2_requestbuffers buf;   
struct v4l2_buffer buf_info; 
void* buffer_start;
struct cam_stream_config *cfg = NULL;
int type;

void *CamStreamerStateMachine(void *args)
{   
    int ret_val = 0; 
    cur_state = UNINIT_CS;
    error_val = 0;
    
    while(1)
    {
        switch(cur_state)
        {
            case UNINIT_CS:
                if (cam_active)
                {
                    cur_state = INIT_CS;
                }

                if (cam_thread_close)
                {
                    // Nothing was set up, so we can just return from the thread.
                    return 0;
                }
                break;

            case INIT_CS:                
                    
                    // cfg = NULL;
                    ret_val = InitModule_CS(cfg);
                    if (ret_val < 0)                
                    {
                        cur_state = ERROR_CS;
                        error_val = ret_val;
                    }               

                    cur_state = INIT_COMP_CS; 
                break;

            case INIT_COMP_CS:
                if (cam_active && !cam_thread_close)
                {
                    cur_state = START_STREAM_CS;
                }
                else
                {
                    cur_state = IDLE_CS;
                }
                
                break;

            case START_STREAM_CS:
                ret_val = BeginStreaming_CS();
                if (ret_val >= 0)
                {
                    error_val = ret_val;
                    cur_state = ERROR_CS;
                }

                cur_state = STREAM_ACTIVE_CS;

                break;

            case STREAM_ACTIVE_CS:        

                if (!cam_active || cam_thread_close)            
                {
                    cur_state = STOP_STREAM_CS;
                }

                ret_val = GetFrame_CS();
                if (ret_val < 0)
                {
                    error_val = ret_val;
                    cur_state = ERROR_CS;
                }

                break;

            case STOP_STREAM_CS:
                ret_val = StopStreaming_CS();
                if (ret_val < 0)
                {
                    error_val = ret_val;
                    cur_state = ERROR_CS;
                }

                cur_state = IDLE_CS;
                break;

            case IDLE_CS:
                if (cam_active && !cam_thread_close)
                {
                    cur_state = START_STREAM_CS;
                }

                if (cam_thread_close)
                {
                    cur_state = DEINIT_CS;
                }
                break;

            case DEINIT_CS:
                ret_val = DeinitModule_CS();
                if (ret_val < 0)
                {
                    error_val = ret_val;
                    cur_state = ERROR_CS;
                }
                return 0;
                break;

            case ERROR_CS:
                ret_val = HandleError_CS(error_val);                        
                return 0;
                break;

            default:
                break;
        }
    }
}

static int InitModule_CS(struct cam_stream_config *config)
{
    fd = open("/dev/video0", O_RDWR);
    if (fd < 0) 
    {
        perror("Open Failed!\n"); 
        return OPENERR_CS;
    }

    if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0)
    {
        perror("Cap query failed.\n");
        return CAPQRYERR_CS;
    }

    // check to ensure camera supports streaming we need
    if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) || !(cap.capabilities & V4L2_CAP_STREAMING))
    {
        perror("Video capture or streaming not supported.\n");
        return STREAMERR_CS;
    }

    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
    format.fmt.pix.width = 1920;
    format.fmt.pix.height = 1080;

    if (ioctl(fd, VIDIOC_S_FMT, &format) < 0)
    {
        perror("Format rejected.\n");
        return FMTERR_CS;
    }

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.count = 1;

    if( ioctl(fd, VIDIOC_REQBUFS, &buf) < 0)
    {
        perror("VIDIOC_REQBUFS\n");
        return REQBUFERR_CS;
    }

    memset(&buf_info, 0, sizeof(buf_info));
    buf_info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf_info.memory = V4L2_MEMORY_MMAP;
    buf_info.index = 0;

    if(ioctl(fd, VIDIOC_QUERYBUF, &buf_info))
    {
        perror("VIDIOC_QUERYBUF\n");
        return QRYBUFERR_CS;
    }

    buffer_start = mmap(
        NULL,
        buf_info.length,
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        fd,
        buf_info.m.offset
    );

    if(buffer_start == MAP_FAILED)
    {
        perror("mmap\n");
        return MEMMAPERR_CS;
    }

    memset(buffer_start, 0, buf_info.length);
    type = buf_info.type;

    // Initialize frame buffer to share with socket thread.
    pthread_mutex_lock(&frame_mtx);
    {
        frame_buf = (char *)malloc(buf_info.length * FRAME_BUF_SZ);
        memset(frame_buf, 0, buf_info.length * FRAME_BUF_SZ);
        frame_det.head = 0;
        frame_det.tail = 0;
        frame_det.length = buf_info.length;
        frame_det.buf_size = buf_info.length * FRAME_BUF_SZ;
    }
    pthread_mutex_unlock(&frame_mtx);
}

static int DeinitModule_CS(void)
{
    // TODO: implement.
    return 0;
}

static int BeginStreaming_CS(void)
{
    if(ioctl(fd, VIDIOC_STREAMON, &type) < 0)
    {
        perror("VIDIOC_STREAMON\n");
        return -1;
    }
    
    // while(1)
    // {
    //     if(ioctl(fd, VIDIOC_QBUF, &buf_info) < 0)
    //     {
    //         perror("VIDIOC_QBUF\n");
    //         return -1;
    //     }

    //     if(ioctl(fd, VIDIOC_DQBUF, &buf_info) < 0)
    //     {
    //         perror("VIDIOC_DQBUF\n");
    //         return -1;
    //     }       
    // }    
}

static int GetFrame_CS(void)
{
    int ret_val = 0;
    ret_val = ioctl(fd, VIDIOC_QBUF, &buf_info);
    if (ret_val < 0)
    {
        return ret_val;
    }

    ret_val = ioctl(fd, VIDIOC_DQBUF, &buf_info);

    if(ret_val < 0)
    {
        return ret_val;
    }       

    // TODO: Queue frame for socket thread!
    pthread_mutex_lock(&frame_mtx);
    // Insert frame fi there's room, otherwise, start dropping frames until the buffer is cleared.
    if (frame_det.tail + buf_info.length < frame_det.buf_size)
    {
        memcpy(&frame_buf[frame_det.tail], buffer_start, buf_info.length);
    }

    frame_det.tail += buf_info.length;

    pthread_mutex_unlock(&frame_mtx);
}

static int StopStreaming_CS(void)
{
    if(ioctl(fd, VIDIOC_STREAMOFF, &type) < 0)
    {
        perror("VIDIOC_STREAMOFF\n");
        return -1;
    }   

    if (close(fd) < 0)
    {
        perror("fd Clost;");
        return -1;
    }
    return 0;
}

static int HandleError_CS(int error_val)
{
    // TODO: implement.
    return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <pthread.h>
#include <cam_streaming.h>
#include <cam_net.h>
#include "main.h"

bool cam_active = false;
bool cam_thread_close = false;
bool sock_active = false;
bool sock_thread_close = false;
char *frame_buf;
struct frame_details frame_det;
pthread_mutex_t frame_mtx = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char *argv[])
{
    int thd_ret;
    pthread_t cam_thread;
    pthread_t sckt_thread; 
    void *cam_ret;
    void *sckt_ret;

    frame_det.head = 0;
    frame_det.tail = 0;
    frame_det.buf_size = 0;
    frame_det.length = 0;

    cam_active = true;
    cam_thread_close = false;
    sock_active = true;
    sock_thread_close = false;

    thd_ret = pthread_create(&cam_thread, NULL, CamStreamerStateMachine, NULL);
    if (thd_ret != 0)
    {
        perror("PThread create camera.\n");
        return -1;
    }

    thd_ret = pthread_create(&sckt_thread, NULL, SocketStateMachine_CN, NULL);
    if (thd_ret != 0)
    {
        perror("pthread create socket.\n");
    }

    sleep(3);
    cam_active = false;
    cam_thread_close = true;
    sock_active = false;
    sock_thread_close = true;

    thd_ret = pthread_join(cam_thread, &cam_ret);
    if (thd_ret != 0)
    {
        perror("Cam Thread join\n");
        return -1;
    }

    thd_ret = pthread_join(sckt_thread, &sckt_ret);
    if (thd_ret != 0)
    {
        perror("socket thread join.\n");
        return -1;
    }

    printf("Main exiting\n");
}

// int main(int argc, char *argv[])
// {
//     int fd;
//     struct v4l2_capability cap;
//     struct v4l2_format format;
//     struct v4l2_requestbuffers buf;   
//     struct v4l2_buffer buf_info;

//     pthread_t cam_thread;
//     pthread_t skt_thread; 

//     /*
//     =======================================
//     Video Init
//     =======================================
//     */
//     fd = open("/dev/video0", O_RDWR);
//     if (fd < 0) 
//     {
//         perror("Open Failed!\n"); 
//         exit(1);
//     }

//     if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0)
//     {
//         perror("Cap query failed.\n");
//         exit(1);
//     }

//     // check to ensure camera supports streaming we need
//     if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) || !(cap.capabilities & V4L2_CAP_STREAMING))
//     {
//         perror("Video capture or streaming not supported.\n");
//         exit(1);
//     }

//     format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//     format.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
//     format.fmt.pix.width = 1920;
//     format.fmt.pix.height = 1080;

//     if (ioctl(fd, VIDIOC_S_FMT, &format) < 0)
//     {
//         perror("Format rejected.\n");
//         exit(1);
//     }

//     buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//     buf.memory = V4L2_MEMORY_MMAP;
//     buf.count = 1;

//     if( ioctl(fd, VIDIOC_REQBUFS, &buf) < 0)
//     {
//         perror("VIDIOC_REQBUFS\n");
//         exit(1);
//     }

//     memset(&buf_info, 0, sizeof(buf_info));
//     buf_info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
//     buf_info.memory = V4L2_MEMORY_MMAP;
//     buf_info.index = 0;

//     if(ioctl(fd, VIDIOC_QUERYBUF, &buf_info))
//     {
//         perror("VIDIOC_QUERYBUF\n");
//         exit(1);
//     }

//     void* buffer_start = mmap(
//         NULL,
//         buf_info.length,
//         PROT_READ | PROT_WRITE,
//         MAP_SHARED,
//         fd,
//         buf_info.m.offset
//     );

//     if(buffer_start == MAP_FAILED)
//     {
//         perror("mmap\n");
//         exit(1);
//     }

//     memset(buffer_start, 0, buf_info.length);

//     int type = buf_info.type;

//     /* 
//     ===================================
//     SDL Init
//     ===================================
//     */

//     if (SDL_Init(SDL_INIT_VIDEO) < 0)
//     {
//         perror("SDL_init\n");
//         exit(1);
//     }
//     if (IMG_Init(IMG_INIT_JPG) < 0)
//     {
//         perror("IMG_Init\n");
//         exit(1);
//     }

//     SDL_Window *screen = NULL;
//     SDL_Renderer *renderer = NULL;
//     SDL_Texture *tex = NULL;
//     SDL_CreateWindowAndRenderer(format.fmt.pix.width, format.fmt.pix.height, 0, &screen, &renderer);

//     if (screen == NULL || renderer == NULL)
//     {
//         perror("SDL_CreateWindowAndRenderer\n");
//         exit(1);
//     }

//     // Draw black to the screen
//     SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
//     SDL_RenderClear(renderer);
//     SDL_RenderPresent(renderer);

//     // tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YUY2, SDL_TEXTUREACCESS_STREAMING, format.fmt.pix.width, format.fmt.pix.height);


//     if(ioctl(fd, VIDIOC_STREAMON, &type) < 0)
//     {
//         perror("VIDIOC_STREAMON\n");
//         exit(1);
//     }

//     int iters = 0;
//     while(iters < 100)
//     {
//         if(ioctl(fd, VIDIOC_QBUF, &buf_info) < 0)
//         {
//             perror("VIDIOC_QBUF\n");
//             exit(1);
//         }

//         if(ioctl(fd, VIDIOC_DQBUF, &buf_info) < 0)
//         {
//             perror("VIDIOC_DQBUF\n");
//             exit(1);
//         }

//         SDL_RWops *buf_stream = SDL_RWFromMem(buffer_start, buf_info.length);
//         SDL_Surface *frame = IMG_Load_RW(buf_stream, 0);
//         tex = SDL_CreateTextureFromSurface(renderer, frame);

//         SDL_RenderClear(renderer);
//         SDL_RenderCopy(renderer, tex, NULL, NULL);
//         SDL_RenderPresent(renderer);

//         SDL_DestroyTexture(tex);
//         SDL_FreeSurface(frame);
//         SDL_RWclose(buf_stream);

//         iters++;
//     }    

//     if(ioctl(fd, VIDIOC_STREAMOFF, &type) < 0)
//     {
//         perror("VIDIOC_STREAMOFF\n");
//         exit(1);
//     }

//     sleep(2);

//     close(fd);
//     return EXIT_SUCCESS;
// }